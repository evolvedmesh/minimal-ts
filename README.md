# minimal-ts

Starter development template for typescript projects (not optimized for frontend).

## Pre-requisites
Global npm packages that must be installed before use.
- pnpm
- nodemon
- rimraf
- esbuild
- eslint

## Recommended VSCode settings
- Enable auto-formatting on save
